/**
 * Created by User on 29.11.14.
 */
$(document).ready(function () {
    $('.sub_block_1 .span4').show(1200);
    $('.sub_block_2').animate({
        opacity: "show"
    }, 4000);
   /* $('#myCarousel').animate({
        height: 'show'
    }, 1500);*/
    $('body br:first-child').hide();
});

function add_new_post() {
    var form = $('#new-post-form');
    var modal_success_text = $('#modal-success');
    var data = form.serializeArray();
    var arg1 = data[0].value;
    var arg2 = data[1].value;
    var arg3 = data[2].value;
    var arg4 = data[3].value;
    jQuery.ajax({
        url: window.location.href + 'ajax/postadd',
        type: 'POST',
        data: 'arg1=' + arg1
            + '&arg2=' + arg2
            + '&arg3=' + arg3
            + '&arg4=' + arg4,
        beforeSend: function (data) { // событие до отправки
            form.addClass('ajax-loader');
            $('.add_new_post').button('Sending...')
        },
        success: function (data) {
            if (data['error']) {
                modal_success_text.empty();
                modal_success_text.append('<p class="error-msg">Error: ' + data['error'] + '</p>');
            } else {
                /*removed loader*/
                form.removeClass('ajax-loader');
                /*Hide add new post form*/
                $('.add-new-post-form').animate({
                    height: 'hide'
                }, 1500);
                /*Returned standart button*/
                $('.add_new_post').button('reset');
                /*Shoe success record*/
                modal_success_text.empty();
                modal_success_text.append('<p class="success-msg">Спасибо ' + name + ', ваше сообщение принято и будет добавлено после подтверждения.</p>');
                $('#modal').modal({keyboard: true, show: true});
                //Reset all data in form
                form.trigger('reset');
            }

        }
    });
}
function reload_blog_posts() {


}
function load_default_ajax_content(url) {
    sendAjax(url);
}
function load_content(url) {
    sendAjax(url);
}
/**
 * Created by Andrey Bondarenko on 24.11.14.
 */


function sendData() {
    /*    var vip_status = jQuery('.first_block .checkbox');
     var data_field = jQuery('.data_field');
     if (data_field[0].value) {
     error('hide', '2000');
     sendAjax(data_field[0].value, vip_status[0].checked)
     } else {
     error('show')
     }*/
}
/**
 * Block when Ajax
 */
function formBlock() {
    jQuery('.ajax-response').each(function () {
        this.disabled = true;
    });
    jQuery('.ajax-response').addClass('ajax-loader');
    if (jQuery('.ajax-block-response').length > 0) {
        jQuery('.ajax-block-response').animate({
            height: "hide"
        }, 1200);
    }

}

/**
 * Unblock when Ajax success
 */
function formUnBlock() {
    jQuery('.ajax-response').each(function () {
        this.disabled = false;
    });
    jQuery('.ajax-response').removeClass('ajax-loader');
    jQuery('.ajax-block-response').animate({
        height: "show"
    }, 1200);

}

/**
 * send ajax query on main.php
 * @param data_array
 */
function sendAjax(url) {
    formBlock();
    jQuery.ajax({
        url: url,
        type: 'POST',
        //data: 'request=' + request_params + '&vip=' + vip,
        success: function (html) {
            jQuery('.ajax-response').empty();
            jQuery('.ajax-response').append(jQuery(html));
            //jQuery('.ajax-results, .ajax-results .row-fluid').show(1000);
            formUnBlock();
        }
    });
}
