<?php

/**
 * Created by PhpStorm.
 * User: shekler
 * Date: 08.09.14
 * Time: 0:25
 */
final class Database
{
    /**
     * @var
     */
    protected static $host;
    /**
     * @var
     */
    protected static $userName;
    /**
     * @var
     */
    protected static $userPassword;
    /**
     * @var
     */
    protected static $dataBaseName;

    protected static $_installErrors = array();

    /**
     * @param $dbHost
     * @param $dbUserName
     * @param $dbUserPassword
     * @param $dbName
     */
    public function __construct($dbHost, $dbUserName, $dbUserPassword, $dbName)
    {
        self::$host = $dbHost;
        self::$userName = $dbUserName;
        self::$userPassword = $dbUserPassword;
        self::$dataBaseName = $dbName;
    }

    /**
     * start install process
     */
    public function runInstall()
    {
        try {
            if (self::$host && self::$userName && self::$userPassword && self::$dataBaseName) {
                $this->installDataBase();
                return true;
            }
        } catch (Exception $e) {
            echo $e;
        }

    }

    public static function installConnectDataBase()
    {
        $paramsArray = self::readConfig();
        $dbUserName = $paramsArray['name'];
        $dbUserPassword = $paramsArray['password'];
        $dbName = $paramsArray['dbname'];
        $dbHost = $paramsArray['host'];
        $connect = mysql_connect($dbHost, $dbUserName, $dbUserPassword) or die(mysql_error());
        if ($connect) {
            mysql_select_db($dbName);

        }
    }

    protected function createConfig()
    {
        try {
            $xml = new DomDocument('1.0', 'utf-8');
            /**
             * create teg 'install'
             */
            $install = $xml->appendChild($xml->createElement('install'));

            /**------------------------------
             * create teg 'user' in 'install'
             */
            $user = $install->appendChild($xml->createElement('userdb'));
            /**
             * create teg 'name' in 'user'
             */
            $name = $user->appendChild($xml->createElement('name'));
            /**
             *  add value in teg name
             */
            $name->appendChild($xml->createTextNode(self::$userName));
            /**
             * create teg 'password' in 'user'
             */
            $pass = $user->appendChild($xml->createElement('password'));
            /**
             * add value in teg password
             */
            $pass->appendChild($xml->createTextNode(self::$userPassword));


            /**----------------------------------
             * create teg 'database' in 'install'
             */
            $dbparams = $install->appendChild($xml->createElement('database'));
            /**
             * create teg 'dbname' in 'database'
             */
            $namedb = $dbparams->appendChild($xml->createElement('dbname'));
            /**
             * add value in dbname
             */
            $namedb->appendChild($xml->createTextNode(self::$dataBaseName));
            /**
             * create teg 'host' in 'database'
             */
            $hostadress = $dbparams->appendChild($xml->createElement('host'));
            /**
             * add value in 'host
             */
            $hostadress->appendChild($xml->createTextNode(self::$host));

            $xml->formatOutput = true;
            $xml->save('config.xml');
        } catch (Exception $e) {
            echo $e;
        }
    }

    /**
     * @param $databaseName
     */
    protected function installDataBase()
    {
        try {
            $connect = mysql_connect(self::$host, self::$userName, self::$userPassword) or die(mysql_error());
            if ($connect) {
                $installDB = mysql_query('CREATE DATABASE IF NOT EXISTS `' . self::$dataBaseName . '`') or die(mysql_error());
                if ($installDB) {
                    mysql_select_db(self::$dataBaseName);
                    $this->createConfig();
                    $this->_installTableInDataBase('test_1.sql');
                }
            }
        } catch (Exception $e) {
            echo $e;
        }

    }

    /**
     * Array for sql query install database table
     * @return array
     */


    /**
     * Method for read config.xml file
     * @return array
     */
    public static function readConfig()
    {
        //TODO: Method not finished
        $config = 'config.xml';

        $paramsArray = array();
        $params = simplexml_load_file($config);

        $params->userdb[0]->name;
        $params->userdb[0]->password;
        $params->database[0]->dbname;
        $params->database[0]->host;

        foreach ($params->userdb as $key) {
            $paramsArray['name'] = $key->name;
            $paramsArray['password'] = $key->password;


        }
        foreach ($params->database as $key) {
            $paramsArray['dbname'] = $key->dbname;
            $paramsArray['host'] = $key->host;


        }

        /**
         * $paramsArray = self::readConfig();
         * $dbUserName = $paramsArray[0];
         * $dbUserPassword = $paramsArray[1];
         * $dbName = $paramsArray[2];
         * $dbHost = $paramsArray[3];
         */
        return $paramsArray;
    }

    protected static function _installTableInDataBase($fileName)
    {
        $filename = (string)'sql' . DS . $fileName;
        if (file_exists($filename)) {
            /**
             * Temporary variable, used to store current query
             */
            $templine = '';
            /**
             * Read in entire file
             */
            $lines = file($filename);
            /**
             * Loop through each line
             */
            foreach ($lines as $line) {
                /**
                 * Skip it if it's a comment
                 */
                if (substr($line, 0, 2) == '--' || $line == '')
                    continue;
                /**
                 * Add this line to the current segment
                 */
                $templine .= $line;
                /**
                 * If it has a semicolon at the end, it's the end of the query
                 */
                if (substr(trim($line), -1, 1) == ';') {
                    /**
                     * Perform the query
                     */
                    mysql_query($templine);
                    /**
                     * Reset temp variable to empty
                     */
                    $templine = '';
                }
            }
        }
    }
}