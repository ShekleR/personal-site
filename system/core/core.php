<?php

/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 8/6/14
 * Time: 3:49 PM
 */
require_once 'core_require_files.php';

final class Core
{
    /**for model Obj(getModel method)
     * @var null
     */
    private static $modelObj = null;

    /**
     * @var array
     */
    protected static $_globalVariable = array();

    protected static $_configFile = 'config.xml';

    protected static $_routeParam = array();

    /**
     * First router logic
     */
    public static function run()
    {
        $paramsArray = Database::readConfig();
        if ((!mysql_select_db($paramsArray['dbname']) && (file_exists(self::$_configFile)))) {
            /**
             * Installed connect with MySql server
             */
             Database::installConnectDataBase();

        }

        $database = new SafeMySQL(array(
            'host'      => $paramsArray['host'],
            'user' => $paramsArray['name'],
            'pass' => $paramsArray['password'],
            'db' => $paramsArray['dbname'],
        ));
        self::setVariable('_database', $database);
        /**
         * Start Routing
         */
        self::_start();
    }

    /**
     * router query
     * this method need finished
     */
    protected static function _start()
    {
        /**
         *  Main controller and action
         */
        $controller_name = 'Index';
        $action_name = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        /**
         * Name controller with URl
         */
        if (!empty($routes[1])) {
            $controller_name = $routes[1];
        }

        /**
         * Name action with url
         */
        if (!empty($routes[2])) {
            $action_name = $routes[2];
        }

        /**
         * Routing to install
         */
        if (!file_exists(self::$_configFile)) {
            $controller_name = 'Install';
        }

        self::$_routeParam['_controller'] = $controller_name;
        self::$_routeParam['_action'] = $action_name;
        $model_name = 'Model_' . $controller_name;
        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'action_' . $action_name;


        /**
         * add files
         */

        /*$model_file = strtolower($model_name) . '.php';
        $model_path = "application/code/models/" . $model_file;
        if (file_exists($model_path)) {
            include "application/code/models/" . $model_file;
        }*/

        /**
         * add files controller
         */
        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = "application/code/controllers/" . $controller_file;
        if (file_exists($controller_path)) {
            include "application/code/controllers/" . $controller_file;
        } else {
            self::_ErrorPage404();
        }

        /**
         * create controller object
         */
        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            /**
             * call action in controller
             */
            $controller->$action();
        } else {

            self::_ErrorPage404();
        }
    }

    /**
     * redirect on 404 page
     */
    protected static function _ErrorPage404()
    {
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        self::redirect('404');
    }

    /**
     * redirect to url for onclick in Html
     * @param $arg
     */
    public static function onClickRedirect($controller = null, $action = null, $params = array())
    {
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                //TODO: insert code for get params
            }
        } else {
            return "javascript:location.href='http://" . self::siteUrl() . "/" . $controller . "/" . $action . "'";
        }
    }

    public static function getUrl($url = null, $params = array())
    {
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                //TODO: insert code for get params
            }
        } else {
            return "http://" . self::siteUrl() . "/" . $url;
        }
    }

    /**
     * return template adress
     * @param $name
     * @return string
     */
    public static function getTemplateToName($name)
    {

        $template = 'application/code/views/' . $name;
        if (!file_exists($template)) {
            self::addMsg()->setErrors('Template ' . $name . ' is not found');
        }
        return $template;
    }

    /**returned model obj
     *
     * @param $nameModel
     * @return null
     */
    public static function getModel($nameModel)
    {
        $model_name = 'Model_' . $nameModel;
        $model_file = strtolower($model_name) . '.php';
        if (file_exists("application/code/models/" . $model_file)) {
            include "application/code/models/" . $model_file;
        } else {
            return 'File not found';
        }
        self::$modelObj[$nameModel] = new $model_name;
        return self::$modelObj[$nameModel];
    }


    public function getSystemModel($nameSystemModel)
    {
        $model_name = 'Model_' . $nameSystemModel;
        $model_file = strtolower($model_name) . '.php';
        $model_path = "application/system/models/" . $model_file;
        if (file_exists($model_path)) {
            include "application/system/models/" . $model_file;

            $systemModelObject = new $model_name;
            return $systemModelObject;
        } //TODO:exception : model not found
        else {
            return 'file not found';
        }

    }

    /**
     * @param $key
     * @param $value
     */
    public static function setVariable($key, $value)
    {
        //TODO:: not ended
        self::$_globalVariable[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function getVariable($key)
    { //TODO:: not ended
        return self::$_globalVariable[$key];
    }

    /**
     * @param $nameModel
     * @return mixed
     */
    public static function getSingleton($nameModel)
    {
        if (empty(self::$_globalVariable[$nameModel])) {
            self::$_globalVariable[$nameModel] = self::getModel($nameModel);
        }
        return self::$_globalVariable[$nameModel];
    }

    /**
     * return sire url
     * @return mixed
     */
    public static function siteUrl()
    {
        return $_SERVER['HTTP_HOST'];
    }

    /**
     * @return bool|string
     */
    public static function getCurrentDate()
    {
        return date('Y:m:d H:i:s');
    }

    public static function addMsg()
    {
        if (empty(self::$_globalVariable['msg'])) {
            self::$_globalVariable['msg'] = new Message();
        }
        return self::$_globalVariable['msg'];
    }

    /**
     * @param $url
     */
    public static function redirect($url = null)
    {
        $host = 'http://' . self::siteUrl() . '/' . $url;
        header('Location:' . $host);
    }

    public static function baseDirPath($alias)
    {
        if ($alias) {
            switch ($alias) {
                case 'js':
                    return (string)DS . 'skin' . DS . 'js';
                    break;
                case 'css':
                    return (string)DS . 'skin' . DS . 'css';
                    break;
                case 'img':
                    return (string)DS . 'media';
                    break;
            }
        }
    }

    /**
     * @param null $param
     * @return array|null
     */
    public static function getParams($param = null)
    {
        if (isset(self::$_routeParam)) {
            if ($param) {
                return self::$_routeParam[$param];
            }
            return self::$_routeParam;
        }
        return null;
    }
}