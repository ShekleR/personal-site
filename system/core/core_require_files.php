<?php
/**
 * Created by PhpStorm.
 * User: shekler
 * Date: 13.09.14
 * Time: 23:35
 */

/**
 * core Files
 */
require_once 'core.php';
require_once 'core_controller.php';
require_once 'core_model.php';
require_once 'core_view.php';
require_once 'database/database.php';


/**
 * library files
 */
require_once './lib/Zend/Debug.php';
require_once './lib/message.php';
require_once './lib/safemysql.class.php';