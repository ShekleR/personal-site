<?php

/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 7/18/14
 * Time: 19:30 PM
 */

/**
 * This main class fore all view
 *
 * Class Core_View
 */
class Core_View
{
    /**
     * main template for all page
     */
    protected $mainTemplateView;

    /**Child html
     * @var array
     */
    protected $setChildTemplate = array();

    protected $getData = null;

    /**
     * set page title
     * @var string
     */
    public $pageTitle;

    /**
     * Set unique body class
     * @var
     */
    public $bodyClass;


    public function __construct()
    {
        /**
         * default maun template
         */
        $this->mainTemplateView = Core::getTemplateToName('main_template.phtml');
        /**
         * default page title
         */
        $this->pageTitle = 'WebCore v. 1.0 Personal Site Bondarenko A.';
        /**
         * default body class
         */
        $this->bodyClass = 'webcore';
        /**
         *
         */
        $this->_setDefaultChild();

    }

    /**
     * @param null $contentViewFileName if empty, take this params with content child
     * @param null $templateViewAdress
     * @param null $data
     */
    public function generatePage($contentViewFileName = null, $templateViewAdress = null, $data = null)
    {
        if ($data) {
            $this->getData = $data;
        }
        /**
         * set main template
         */
        if ($templateViewAdress) {
            $this->mainTemplateView = $templateViewAdress;
        }
        /**
         * set content
         */
        if ($contentViewFileName) {
            $this->setChildTemplate['content'] = $contentViewFileName;
        }
        /**
         * generation page
         */
        if (file_exists($this->mainTemplateView)) {
            include $this->mainTemplateView;
        } else {
            print_r('Not Founded Main Template:' . $this->mainTemplateView);
        }


    }

    /**
     * set single template full adress
     * @param $adress
     * @param array $data
     */
    public function setSinglePageWithFullAdress($adress, $data = array())
    {
        if (file_exists($adress)) {
            include_once $adress;
        } else {
            //$this->errorMsg('File is not found');
            Core::addMsg()->setErrors('File is not found');

        }

    }

    /**set child template
     * @param $data
     *  'child_name'=>'template/path'
     */
    public function setChild($dataArray = array())
    {
        foreach ($dataArray as $key => $value) {
            $this->setChildTemplate[$key] = $value;

        }
        return $this;
        //TODO: any functional for child

    }

    /**
     * @param $videoName
     * @return string
     */
    protected function outputVideo($videoUrl)
    {
        /*$videoUrl = '/media/video/' . $videoName . '.mp4';*/
        $html = '<object id="video_player" type="application/x-shockwave-flash" data="/skin/player/uppod.swf" width="600" height="400">
                <param name="bgcolor" value="#ffffff" />
                <param name="allowFullScreen" value="true" />
                <param name="allowScriptAccess" value="always" />
                <param name="wmode" value="window" />
                <param name="movie" value="uppod.swf" />
                <param name="flashvars" value="file=' . $videoUrl . '" />
                </object>';
        $testArray = array($html, $html, $html, $html, $html, $html, $html,);
        return $html;
        /*return $html;*/
    }

    protected function getImageUrl($fileName)
    {
        $file = 'skin/images/' . $fileName;
        if (file_exists($file)) {
            return $file;
        } else {
            // $this->errorMsg('File not found');
        }
    }

    protected function getMediaImg($fileName)
    {
        $file = '../../../../media/img/' . $fileName;
        return $file;

    }

    /**
     * include child block
     * @param null $name
     * @return null
     */
    protected function getChild($name = null)
    {
        if ($name) {
            if (!empty($this->setChildTemplate)) {
                if (array_key_exists($name, $this->setChildTemplate)) {
                    if (file_exists($this->setChildTemplate[$name])) {
                        include $this->setChildTemplate[$name];
                    }else{
                        Core::addMsg()->setErrors('Not Founded Template '.$this->setChildTemplate[$name]);
                    }
                } else {
                    //Core::addMsg()->setErrors('Not Founded Child '.$name);
                    return null;
                }
            }
            return true;
        } else {
            return null;
        }
    }

    /**
     * create main child
     */
    protected function _setDefaultChild()
    {
        $this->setChildTemplate['head-link'] = 'application/code/views/html/head_link.phtml';
        $this->setChildTemplate['header'] = 'application/code/views/html/header.phtml';
        $this->setChildTemplate['footer'] = 'application/code/views/html/footer.phtml';
    }

    /**
     * @return null|string
     */
    protected function _getShortMsg()
    {
        $messageArray = Core::addMsg()->getMsgCollection();
        $message = null;
        if (!empty($messageArray)) {
            foreach ($messageArray as $msg) {
                $message .= $msg . '<br>';
            }
        }
        return $message;
    }

    /**
     * @return null|string
     */
    protected function _addedCustomHeaderLinks()
    {
        $js = Core::getVariable('_customHeaderJs');
        $css = Core::getVariable('_customHeaderCss');
        $connectLinks = null;
        if (!empty($js)) {
            if (is_array($js)) {
                foreach ($js as $scriptLink) {
                    $connectLinks .= '<script src="' . $scriptLink . '" type="text/javascript"></script><br>';
                }
            } else {
                $connectLinks .= '<script src="' . $js . '" type="text/javascript"></script>';
            }
        }
        if (!empty($css)) {
            if (is_array($js)) {
                foreach ($css as $styleFile) {
                    $connectLinks .= '<link rel="stylesheet" type="text/css" href="' . $styleFile . '"/><br>';
                }
            } else {
                $connectLinks .= '<link rel="stylesheet" type="text/css" href="' . $css . '"/><br>';
            }
        }
        return $connectLinks;
    }

}
