<?php

/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 7/19/14
 * Time: 12:53 PM
 */
class Core_Controller
{

    private $viewObject = null;

    public function __construct()
    {

    }

    /**
     *
     */
    public function action_index()
    {

    }


    /**
     * returned view object
     * @return Core_View|View
     */
    protected function _getView()
    {
        if (!$this->viewObject) {
            $this->viewObject = new Core_View();
        }
        return $this->viewObject;
    }

    /**
     * @param $jsFileName
     */
    protected function _customHeaderJs($jsFileName)
    {
        Core::setVariable('_customHeaderJs', Core::baseDirPath('js') . DS . $jsFileName);
    }

    /**
     * @param $cssFileName
     */
    protected function _customHeaderCss($cssFileName)
    {
        Core::setVariable('_customHeaderCss', Core::baseDirPath('css') . DS . $cssFileName);
    }
    /**
     * @param $param
     * @return array|null
     */
    protected function _getParam($param)
    {
        $coreParam = Core::getParams($param);
        if (!empty($coreParam)) {
            return $coreParam;
        } elseif (!empty($_POST[$param])) {
            return $_POST[$param];
        }
        return null;
    }
}
