﻿<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 7/20/14
 * Time: 14:20 PM
 */

ini_set('display_errors', 1);
const DS = '/';
$core_class = 'system/core/core.php';

if (file_exists($core_class)) {

    require_once $core_class;
} else {
    echo 'Not found core class';
}
Core::run();


