<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.11.14
 * Time: 23:25
 */
class Message
{

    /**
     * @var array
     */
    protected $_messagesArray = array();

    /**
     * Set short message in header page
     * @param $msgText
     * @return bool
     */
    protected function _setMsg($msgText)
    {
        if ($msgText) {
            array_push($this->_messagesArray, $msgText);
        }
    }

    /**
     * @param $error
     */
    public function setErrors($error)
    {
        $this->_setMsg('<p class="text-error">' . $error . '</p>');
    }

    /**
     * @param $message
     */
    public function setWarning($message)
    {
        $this->_setMsg('<p class="text-warning">' . $message . '</p>');
    }

    /**
     * @param $message
     */
    public function setSuccess($message)
    {
        $this->_setMsg('<p class="text-success">' . $message . '</p>');
    }


    public function getMsgCollection()
    {
        if (!empty($this->_messagesArray)) {
            return $this->_messagesArray;
        } else {
            return null;
        }

    }
}