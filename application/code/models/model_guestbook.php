<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 01.12.14
 * Time: 11:13
 */
class Model_GuestBook extends Core_Model
{

    private $_tableName = 'guest_book';

    public function getAllPosts()
    {
        return array_reverse($this->_databaseObject->getAll('SELECT * FROM `' . $this->_tableName . '`'));
    }

    public function getApprovedPosts()
    {
        return array_reverse($this->_databaseObject->getAll('SELECT * FROM `' . $this->_tableName . '` WHERE `approved`="1"'));
    }

    public function approvedPost($postId, $status)
    {
        if ($status == 'approved') {
            mysql_query("UPDATE `" . $this->_tableName . "` SET `approved`='1' WHERE `id`='" . $postId . "'");

        } elseif ($status == 'disabled') {
            mysql_query("UPDATE `" . $this->_tableName . "` SET `approved`='" . null . "' WHERE `id`='" . $postId . "'");
        } else {
            die('Undefined Params');
        }
    }

    public function addNewPostInDataBase(array $saveData)
    {
        $allowed = array('user_name', 'user_email', 'post', 'rating', 'data');
        $data = $this->_databaseObject->filterArray($saveData, $allowed);
        $sql = "INSERT INTO ?n SET ?u";
        $this->_databaseObject->query($sql, $this->_tableName, $data);
        return true;
    }

}