<?php
/**
 * Created by PhpStorm.
 * User: shekler
 * Date: 07.08.14
 * Time: 0:20
 */

/**IS NOT READY
 * Class Controller_Install
 */
class Controller_Install extends Core_Controller
{

    public function action_index()
    {
        if ((empty($_POST['mysql_host'])) &&
            (empty($_POST['mysql_log'])) &&
            (empty($_POST['mysql_pass'])) &&
            (empty($_POST['mysql_db_name']))
        ) {
            $this->_getView()->generatePage('system/install.phtml');
        } else {
            $dataBaseObject = new Database(
                $_POST['mysql_host'],
                $_POST['mysql_log'],
                $_POST['mysql_pass'],
                $_POST['mysql_db_name']);
            try
            {
                $dataBaseObject->runInstall();
                Core::redirect();
            }catch (Exception $e){

            }
        }
    }
}