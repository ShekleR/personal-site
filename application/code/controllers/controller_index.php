<?php

/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 7/19/14
 * Time: 12:53 PM
 */
class Controller_Index extends Core_Controller
{

    /**
     * index action for site
     */
    public function action_index()
    {
        Core::setVariable('default_ajax_url', Core::getUrl('ajax/about'));
        $this->_initPage()->generatePage();
    }

    /**
     * @return Core_View|View
     */
    protected function _initPage()
    {
        $this->_getView()->pageTitle = 'Personal Site';
        $this->_getView()->setChild(array(
            'navigation'=>Core::getTemplateToName('html/navigation.phtml'),
            'content' => Core::getTemplateToName('content/content.phtml'),
            'block_ajax' => Core::getTemplateToName('content/block_ajax.phtml'),
            'sub_block_1' => Core::getTemplateToName('content/sub_block_1.phtml'),
            'sub_block_2' => Core::getTemplateToName('content/sub_block_2.phtml')));
        return $this->_getView();
    }
}