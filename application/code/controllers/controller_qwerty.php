<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.12.14
 * Time: 23:22
 */

/**
 * Class Controller_Qwerty
 *
 * Admin panel controller
 */
class Controller_Qwerty extends Core_Controller
{

    protected $_log = 'root';
    protected $_pass = 'root';

    public function __construct()
    {
        /**
         * Begin session
         */
        session_start();
        /**
         * Construct Admin Theme
         */
        $this->_initAdminPage();
        /**
         * Check if admin
         */
        if (!$this->_getSession('admin')&&(Core::getParams('_action') != 'authorization')) {
            Core::redirect(parent::_getParam('_controller') . '/authorization');
        }
        $this->_getView()
            ->setChild(
            //set main container 'content' for admin page
                array('content' => Core::getTemplateToName('admin/html/content.phtml')));

    }


    public function action_guestbook()
    {
        $this->pageTitle = 'Admin/GuestBook';
        $this->_getView()
            ->setChild(array('admin_content' => Core::getTemplateToName('admin/guest_book_settings.phtml')
            ))->generatePage();

    }

    public function action_index()
    {
        //Generation page with set child in construct
        $this->_getView()->generatePage();
    }

    /**
     * Created loggin page
     */
    public function action_authorization()
    {
        $this->_getView()->generatePage(Core::getTemplateToName('admin/html/login_page.phtml'));
    }

    /**
     * Check Entered params
     */
    public function action_enter()
    {
        $log = parent::_getParam('admin_login');
        $pass = parent::_getParam('admin_password');
        if (($log) && ($pass)) {
            if (($log = $this->_log) && ($pass == $this->_pass)) {
                $this->_startSession('admin', $log);
                //TODO: Write site log's
                // . session_name() . '=' . session_id()
                Core::redirect(parent::_getParam('_controller') . '/index/?');
            } else {
                print_r('Error');
            }
        } else {
            Core::redirect('777');
        }
    }

    /**
     * Admin Exit
     */
    public function action_destroy()
    {
        $_SESSION = array(); // или $_SESSION = array() для очистки всех данных сессии
        session_destroy();
        Core::redirect($this->_getParam('_controller'));
    }

    public function action_postapproved()
    {
        if ($this->_getParam('approved-guest-book-post')) {
            $id = $this->_getParam('approved-guest-book-post-id');
            if ($id) {
                $status = $this->_getParam('approved-guest-book-post');
                Core::getModel('guestbook')->approvedPost($id, $status);
                Core::redirect(Core::getParams('_controller') . '/guestbook');
            }
        } else {
            Core::redirect('777');
        }
    }

    protected function _initAdminPage()
    {
        $this->_getView()->pageTitle = 'Admin Panel';
        $this->_getView()->bodyClass = 'admin';
        $this->_getView()
            ->setChild(array(
                'header' => Core::getTemplateToName('admin/html/header.phtml'),
                'footer' => Core::getTemplateToName('admin/html/footer.phtml')
            ));
        $this->_customHeaderCss('admin/admin.css');
        $this->_customHeaderJs('admin/admin.js');
        return $this->_getView();
    }

    protected function _startSession($setSession = null, $value = null)
    {
        session_start();
        $_SESSION[$setSession] = $value;

    }

    protected function _getSession($sessionName)
    {
        $session_name = $_SESSION[$sessionName];
        if ($session_name) {
            return $sessionName;
        } else {
            return null;
        }
    }
}