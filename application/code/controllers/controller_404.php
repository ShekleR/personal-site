<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 7/19/14
 * Time: 12:53 PM
 */

class Controller_404 extends Core_Controller
{

    /**
     * generate 404 page if url not-exists
     */
    public function action_index()
    {
        $this->_getView()->pageTitle = '404: page not found!';
        $this->_getView()->generatePage(null, 'system/404.phtml');
    }

}
