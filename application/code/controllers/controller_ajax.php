<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.11.14
 * Time: 10:41
 */
class Controller_Ajax extends Core_Controller
{

    public function action_about()
    {
        $this->_getView()->generatePage(null, Core::getTemplateToName('ajax_content/about_me.phtml'));
    }

    public function action_guest()
    {
        $this->_getView()->generatePage(null, Core::getTemplateToName('ajax_content/guest_book.phtml'));
    }

    public function action_postadd()
    {
        $authorName = (string)$this->_getParam('arg1');
        $authorEmail = (string)$this->_getParam('arg2');
        $rating = (string)$this->_getParam('arg3');
        $postText = (string)$this->_getParam('arg4');
        if ($authorName && $authorEmail && $rating && $postText) {
            $saveData = array(
                'user_name' => $authorName,
                'user_email' => $authorEmail,
                'rating' => $rating,
                'post' => $postText,
                'data'=>date("d.m.Y H:i:s")
            );
            Core::getModel('guestbook')->addNewPostInDataBase($saveData);
        }
    }
}